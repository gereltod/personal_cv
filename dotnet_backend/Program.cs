
using StackExchange.Redis;
using dotnet_backend.API.Infrastructure;
using dotnet_backend.API.DataAccess;
using dotnet_backend.API.Infrastructure.Redis;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Mvc.Formatters;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.Configure<AppSettings>(builder.Configuration.GetSection(nameof(AppSettings)));
builder.Services.AddSingleton<IAppSettings>(sp => sp.GetRequiredService<IOptions<AppSettings>>().Value);

builder.Services.AddSingleton<ConnectionMultiplexer>(sp =>
{
    var settings = sp.GetRequiredService<IOptions<AppSettings>>().Value;
    var configuration = ConfigurationOptions.Parse(settings.ConnectionStringRedis, true);

    configuration.ResolveDns = true;

    return ConnectionMultiplexer.Connect(configuration);
});
builder.Services.AddSingleton<ICache, CacheData>(sp =>
{
    var logger = sp.GetRequiredService<ILogger<CacheData>>();

    var settings = sp.GetRequiredService<IOptions<AppSettings>>().Value;
    var configuration = ConfigurationOptions.Parse(settings.ConnectionStringRedis, true);

    configuration.ResolveDns = true;
    // Redis.InitializeConnectionString(configuration.);
    // Redis.ForceReconnect();
    return new CacheData(logger, ConnectionMultiplexer.Connect(configuration));
});
builder.Services.AddSingleton<ICurrencyQueries, CurrencyQueries>();
builder.Services.AddSingleton<ICoinQueries, CoinQueries>();
builder.Services.AddSingleton<IHttpRequests, HttpRequests>();
builder.Services.AddHostedService<TimedHostedService>();


builder.Services.AddControllers(options =>
{

    options.OutputFormatters.RemoveType<HttpNoContentOutputFormatter>();
});
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

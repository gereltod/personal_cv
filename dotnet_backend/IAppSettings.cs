
public interface IAppSettings
{
    string ConnectionStringRedis { get; set; }
    string ConnectionString { get; set; }

    string ApiUrl { get; set; }
}
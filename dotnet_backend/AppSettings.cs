public class AppSettings : IAppSettings
{
    public string ConnectionStringRedis { get; set; } = String.Empty;
    public string ConnectionString { get; set; } = String.Empty;

    public string ApiUrl { get; set; } = String.Empty;

}
using System.Text.Json.Serialization;

namespace dotnet_backend.API.Models;

public class Currency
{
    public string? code { get; set; }
    public string? symbol { get; set; }
    [JsonNumberHandling(JsonNumberHandling.WriteAsString | JsonNumberHandling.AllowReadingFromString)]
    public decimal rate { get; set; }
    public string? description { get; set; }
}
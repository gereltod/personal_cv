namespace dotnet_backend.API.Models;
public class Coin
{
    public Dictionary<string, string>? time { get; set; }
    public string? disclaimer { get; set; }

    public Dictionary<string, Currency>? bpi { get; set; }
}
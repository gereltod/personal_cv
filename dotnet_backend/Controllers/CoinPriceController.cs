using Microsoft.AspNetCore.Mvc;

using dotnet_backend.API.DataAccess;
using dotnet_backend.API.Infrastructure.Redis;

namespace dotnet_backend.Controllers;


[ApiController]
[Route("[controller]")]
public class CoinPriceController : ControllerBase
{
    private readonly ILogger<CurrencyController> _logger;
    private readonly ICoinQueries _coinQueries;
    public CoinPriceController(ILogger<CurrencyController> logger, ICoinQueries coinQueries)
    {
        _logger = logger;
        _coinQueries = coinQueries;
    }

    [HttpGet(Name = "GetCoinprice")]
    public async Task<IActionResult> Get()
    {
        var response = await _coinQueries.GetCoinListByDate("0");
        return Ok(response);
    }

    [HttpGet]
    [Route("last")]
    public async Task<IActionResult> Last()
    {
        var response = await _coinQueries.GetCoinListByLastPrice();
        return Ok(response);
    }
}

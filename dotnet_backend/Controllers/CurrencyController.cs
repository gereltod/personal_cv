using Microsoft.AspNetCore.Mvc;

using dotnet_backend.API.DataAccess;
using dotnet_backend.API.Infrastructure.Redis;

namespace dotnet_backend.Controllers;


[ApiController]
[Route("[controller]")]
public class CurrencyController : ControllerBase
{
    private readonly ILogger<CurrencyController> _logger;
    private readonly ICurrencyQueries _currencyQueries;
    public CurrencyController(ILogger<CurrencyController> logger, ICurrencyQueries currencyQueries)
    {
        _logger = logger;
        _currencyQueries = currencyQueries;
    }

    [HttpGet(Name = "GetCurrency")]
    public async Task<IActionResult> Get()
    {
        var response = await _currencyQueries.GetCurrencyList();
        return Ok(response);
    }
}

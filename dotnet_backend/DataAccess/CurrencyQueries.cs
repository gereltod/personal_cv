using Microsoft.Extensions.Options;
using System.Text.Json;
using System.Data;
using System.Data.SqlClient;
using dotnet_backend.API.Infrastructure;
using dotnet_backend.API.Infrastructure.Redis;

namespace dotnet_backend.API.DataAccess;

public class CurrencyQueries : ICurrencyQueries
{
    private readonly ILogger<CurrencyQueries> _logger;
    private readonly ICache _cache;
    private readonly IAppSettings _appSettings;
    private string _connectionString = string.Empty;

    public CurrencyQueries(IAppSettings appSettings, ILogger<CurrencyQueries> logger, ICache cache)
    {
        _logger = logger;
        _cache = cache;
        _logger.LogInformation("settings:{appSettings}", appSettings.ConnectionString);

        _appSettings = appSettings;
        _connectionString = !string.IsNullOrWhiteSpace(_appSettings.ConnectionString) ? _appSettings.ConnectionString : throw new ArgumentException(nameof(_appSettings.ConnectionString));
    }
    internal IDbConnection Connection
    {
        get
        {
            return new SqlConnection(_connectionString);
        }
    }

    public async Task<string> GetCurrencyList()
    {
        string? jsonData = await _cache.GetCacheData<string>("global", "userid", "currency_all");

        if (jsonData is null)
        {

            using (IDbConnection connection = Connection)
            {
                string lstResult = String.Empty;
                connection.Open();
                _logger.LogInformation("----- SQL connect command from Currency");

                using (SqlCommand command = new SqlCommand("SELECT * FROM Currency FOR JSON AUTO;", (SqlConnection)connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            lstResult = reader.GetString(0);
                        }
                    }
                }
                if (lstResult != String.Empty)
                {
                    //var jData = JsonSerializer.Serialize(lstResult!);
                    await _cache.SetCacheData<string>("global", "userid", "currency_all", lstResult);
                    return lstResult;
                }
                else
                {
                    return "Empty";
                }
            }

        }
        else
        {
            return jsonData;
        }
    }
}
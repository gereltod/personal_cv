using System;

using dotnet_backend.API.Models;

namespace dotnet_backend.API.DataAccess;

public interface ICoinQueries
{
    Task<string> GetCoinListByDate(string coinId);

    Task<string> GetCoinListByLastPrice();

    int GetMaxIdIntegrationCoinDesk();

    Task<Dictionary<string, Currency>> InsertIntegrationCoinDesk(int maxId, Dictionary<string, Currency> lastPrice);

}
namespace dotnet_backend.API.DataAccess;

public interface ICurrencyQueries
{
    Task<string> GetCurrencyList();
}
using System.Data;
using System.Data.SqlClient;
using System.Text.Json;
using dotnet_backend.API.Models;
using dotnet_backend.API.Infrastructure;
using dotnet_backend.API.Infrastructure.Redis;

namespace dotnet_backend.API.DataAccess;

public class CoinQueries : ICoinQueries
{
    private readonly ILogger<CoinQueries> _logger;
    private readonly ICache _cache;
    private readonly IHttpRequests _request;

    private readonly IAppSettings _appSettings;
    private string _connectionString = string.Empty;

    public CoinQueries(IAppSettings appSettings, ILogger<CoinQueries> logger, ICache cache, IHttpRequests request)
    {
        _logger = logger;
        _cache = cache;
        _request = request;
        _appSettings = appSettings;
        _connectionString = !string.IsNullOrWhiteSpace(_appSettings.ConnectionString) ? _appSettings.ConnectionString : throw new ArgumentException(nameof(_appSettings.ConnectionString));
    }
    internal IDbConnection Connection
    {
        get
        {
            return new SqlConnection(_connectionString);
        }
    }

    public async Task<string> GetCoinListByLastPrice()
    {
        //get from cache
        string? jsonData = await _cache.GetCacheData<string>("global", "coinId", "coin_lastPrice");

        if (jsonData is null)
        {

            using (IDbConnection connection = Connection)
            {
                string lstResult = String.Empty;
                connection.Open();

                using (SqlCommand command = new SqlCommand("SELECT * FROM CoinPrice Where maxID=(select top 1 maxID from  CoinPrice Order by maxID Desc) FOR JSON PATH", (SqlConnection)connection))
                {
                    command.CommandTimeout = 100000;
                    using (var rdr = await command.ExecuteReaderAsync())
                    {
                        while (await rdr.ReadAsync())
                        {
                            lstResult += rdr.GetString(0);
                        }
                    }
                }
                if (lstResult != String.Empty)
                {
                    //var jData = JsonSerializer.Serialize(lstResult!);
                    await _cache.SetCacheData<string>("global", "coinId", "coin_lastPrice", lstResult);
                    return lstResult;
                }
                else
                {
                    return "Empty";
                }
            }
        }
        else
        {
            return jsonData;
        }
    }

    public async Task<string> GetCoinListByDate(string coinId)
    {
        //get from cache
        string? jsonData = await _cache.GetCacheData<string>("global", "coinId", "coin_last");

        if (jsonData is null)
        {

            using (IDbConnection connection = Connection)
            {
                string lstResult = String.Empty;
                connection.Open();

                using (SqlCommand command = new SqlCommand("SELECT * FROM(select top 15 * from  CoinPrice Order by maxID Desc) c FOR JSON PATH", (SqlConnection)connection))
                {
                    command.CommandTimeout = 100000;
                    using (var rdr = await command.ExecuteReaderAsync())
                    {
                        while (await rdr.ReadAsync())
                        {
                            lstResult += rdr.GetString(0);
                        }
                    }
                }
                if (lstResult != String.Empty)
                {
                    //var jData = JsonSerializer.Serialize(lstResult!);
                    await _cache.SetCacheData<string>("global", "coinId", "coin_last", lstResult);
                    return lstResult;
                }
                else
                {
                    return "Empty";
                }
            }

        }
        else
        {
            return jsonData;
        }
    }

    /// <summary>
    /// Read last maxId from DB
    /// </summary>
    /// <returns>return maxID</returns>
    public int GetMaxIdIntegrationCoinDesk()
    {
        int ret = -1;

        using (IDbConnection connection = Connection)
        {
            string lstResult = String.Empty;
            connection.Open();

            using (SqlCommand command = new SqlCommand("SELECT Isnull(Max(maxID),1) as MaxID FROM CoinPrice", (SqlConnection)connection))
            {
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        ret = reader.GetInt32(0);
                    }
                }
            }

        }

        return ret;
    }

    /// <summary>
    /// Read currency List from DB
    /// </summary>
    /// <param name="currencyCode">currency code (USD,)</param>
    /// <returns></returns>
    public async Task<int> GetCurrencyID(string currencyCode)
    {
        int currencyId = 1;
        using (IDbConnection connection = Connection)
        {
            string lstResult = String.Empty;
            connection.Open();

            using (SqlCommand cmd = new SqlCommand("SELECT CurrencyID FROM Currency WHERE Upper(Code)=Upper('" + currencyCode + "')", (SqlConnection)connection))
            {
                using (var reader = await cmd.ExecuteReaderAsync(CommandBehavior.SequentialAccess))
                {
                    if (await reader.ReadAsync())
                    {
                        currencyId = reader.GetInt32(0);
                    }
                }
            }
        }
        return currencyId;
    }

    public async Task<Dictionary<string, Currency>> InsertIntegrationCoinDesk(int maxId, Dictionary<string, Currency> lastPrice)
    {
        Dictionary<string, Currency> ret = new Dictionary<string, Currency>();

        //read from coinprice data CoinDesc API
        var btc_price = await _request.GetCallAPI<Coin>(_appSettings.ApiUrl);
        if (btc_price != null)
        {

            if (btc_price.bpi != null)
            {

                bool isChange = false;

                DateTime dt = DateTime.Now;
                foreach (var bpi in btc_price.bpi.Values)
                {
                    ret.Add(bpi.code!, bpi);

                    int currencyID = await GetCurrencyID(bpi.code!);
                    decimal exchange = 0;
                    if (lastPrice.ContainsKey(bpi.code!))
                    {
                        //calculate to change of previous price 
                        exchange = (((Currency)lastPrice[bpi.code!]).rate - bpi.rate) / 100;
                        if (exchange != 0)
                        {
                            //if price change 
                            isChange = true;

                            //insert changed coin information
                            string sql = "INSERT INTO CoinPrice(CoinID, bpi, maxID, CurrencyID, CurrencyCode, Rate,Change,DateStamp) Values(1,N'"
                    + "'," + maxId + "," + currencyID + ",'" + bpi.code + "'," + bpi.rate + "," + exchange + ",'" + dt.ToString("yyyy/MM/dd HH:mm:ss.mmm") + "')";
                            using (IDbConnection connection = Connection)
                            {
                                connection.Open();
                                using (SqlCommand command = new SqlCommand(sql, (SqlConnection)connection))
                                {
                                    await command.ExecuteNonQueryAsync();
                                }
                            }
                        }
                    }


                }

                if (isChange)
                {
                    //clear all cache
                    await _cache.DeleteCacheData("global", "coinId:coin_last");
                    await _cache.DeleteCacheData("global", "coinId:coin_lastPrice");
                    var jsData = await Task.Run(() => JsonSerializer.Serialize(ret));
                    //use price change to triggered redis pub/sub
                    await _cache.SendPub<string>("socket", jsData);
                }

            }

        }
        return ret;
    }
}
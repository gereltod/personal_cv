namespace dotnet_backend.API.Infrastructure;


public interface IHttpRequests
{
    Task<T?> GetCallAPI<T>(string url);
}
using System.Text.Json;
using StackExchange.Redis;

namespace dotnet_backend.API.Infrastructure.Redis;

public class CacheData : ICache
{
    private readonly ConnectionMultiplexer _redis;
    private readonly IDatabase _database;

    public CacheData(ILogger<CacheData> logger, ConnectionMultiplexer redis)
    {
        _redis = redis;
        _database = _redis.GetDatabase();
    }

    public async Task<bool> DeleteCacheData(string globalKey, string userId)
    {
        bool ret = true;
        await _database.HashDeleteAsync((new RedisKey(globalKey)), (new RedisValue(userId)));
        return ret;
    }

    public async Task<T?> GetCacheData<T>(string globalKey, string userId, string key)
    {
        RedisValue redisV = new RedisValue(userId + ":" + key);
        var jsonData = await _database.HashGetAsync((new RedisKey(globalKey)), redisV);

        if (jsonData.IsNull)
        {
            return default(T);
        }

        return await Task.Run(() => JsonSerializer.Deserialize<T>(jsonData));
    }

    public async Task SetCacheData<T>(string globalKey, string userId, string key, T data)
    {
        var jsonData = await Task.Run(() => JsonSerializer.Serialize<T>(data));
        RedisValue redisV = new RedisValue(userId + ":" + key);
        await _database.HashSetAsync((new RedisKey(globalKey)), redisV, jsonData);
    }

    public async Task SendPub<T>(string channel, T data)
    {
        var jsonData = await Task.Run(() => JsonSerializer.Serialize<T>(data));
        RedisChannel redisCh = new RedisChannel(channel, RedisChannel.PatternMode.Auto);
        await _database.PublishAsync(redisCh, jsonData);
    }
}

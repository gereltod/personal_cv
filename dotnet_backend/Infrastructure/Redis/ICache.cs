
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace dotnet_backend.API.Infrastructure.Redis;

public interface ICache
{
    Task<T?> GetCacheData<T>(string globalKey, string userId, string key);
    Task SetCacheData<T>(string globalKey, string userId, string key, T data);
    Task<bool> DeleteCacheData(string globalKey, string userId);

    Task SendPub<T>(string channel, T data);
}
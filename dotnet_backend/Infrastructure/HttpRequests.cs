
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Net;

namespace dotnet_backend.API.Infrastructure;

public class CultureSpecificQuotedDecimalConverter : JsonConverter<decimal>
{
    public override decimal Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        if (reader.TokenType == JsonTokenType.String)
        {
            return Convert.ToDecimal(reader.GetString());
        }
        else
        {
            return reader.GetInt32();
        }
    }

    public override void Write(Utf8JsonWriter writer, decimal value, JsonSerializerOptions options)
    {
        throw new NotImplementedException();
    }

}

public class HttpRequests : IHttpRequests
{


    /// <summary>
    /// All HTTP Get method use
    /// </summary>
    /// <param name="url"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public async Task<T?> GetCallAPI<T>(string url)
    {
        var jsonOptions = new JsonSerializerOptions()
        {
            NumberHandling = JsonNumberHandling.WriteAsString | JsonNumberHandling.AllowReadingFromString
        };
        //     {
        //         NumberHandling = JsonNumberHandling.AllowReadingFromString |
        //  JsonNumberHandling.WriteAsString
        //     };

        jsonOptions.Converters.Add(new CultureSpecificQuotedDecimalConverter());
        T res;
        using (HttpClient client = new HttpClient())
        {
            var response = await client.GetAsync(url);
            //TODO
            //add authentication

            if (response != null)
            {
                var jsonString = await response.Content.ReadAsStringAsync();

                res = JsonSerializer.Deserialize<T>(jsonString, jsonOptions)!;
                return res;
            }
        }
        return default(T);
    }
}
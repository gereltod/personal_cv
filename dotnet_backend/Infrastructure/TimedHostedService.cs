using dotnet_backend.API.DataAccess;
using dotnet_backend.API.Models;
using dotnet_backend.API.Infrastructure;
using dotnet_backend.API.Infrastructure.Redis;

namespace dotnet_backend.API.Infrastructure;

public class TimedHostedService : IHostedService, IDisposable
{
    private int executionCount = 0;
    private Dictionary<string, Currency> _lastPrice;
    private readonly ILogger<TimedHostedService> _logger;
    private readonly ICache _cache;
    private readonly ICoinQueries _coin;
    private Timer _timer = null!;

    public TimedHostedService(ILogger<TimedHostedService> logger, ICache cache, ICoinQueries coin)
    {
        _logger = logger;
        _cache = cache;
        _coin = coin;
        _lastPrice = new Dictionary<string, Currency>();

        executionCount = coin.GetMaxIdIntegrationCoinDesk();
    }

    public Task StartAsync(CancellationToken stoppingToken)
    {
        _logger.LogInformation("Timed Hosted Service running.");

        //try to get data from API every 40 seconds 
        _timer = new Timer(DoWork, null, TimeSpan.Zero,
            TimeSpan.FromSeconds(40));

        return Task.CompletedTask;
    }

    private async void DoWork(object? state)
    {

        var count = Interlocked.Increment(ref executionCount);
        _logger.LogInformation(
            "Timed Hosted Service is working. Count: {Count}", count);

        var lastPrice = await _coin.InsertIntegrationCoinDesk(count, _lastPrice);
        _lastPrice = lastPrice;

    }

    public Task StopAsync(CancellationToken stoppingToken)
    {
        _logger.LogInformation("Timed Hosted Service is stopping.");

        _timer?.Change(Timeout.Infinite, 0);

        return Task.CompletedTask;
    }

    public void Dispose()
    {
        _timer?.Dispose();
    }
}
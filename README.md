
Backend (https://api.oju.mn/coinprice) (if you push to branch production, it will publish to automatically digital ocean server)
- dotnet core 6.0 (https://gitlab.com/gereltod/personal_cv/)

Mobile (https://github.com/gereltod/btc-app) branch:dev (Please find attached file that is demo mobile gif)

Small errors are possible, especially in the backend's pipeline. 

## Desing

![Alt text](./dotnet_backend/architecture/btc_design.drawio.png "Architecture diagram")

## Database

![Alt text](./dotnet_backend/architecture/btc_db_diagram.png "Database diagram")

## Mobile

![Alt text](./dotnet_backend/architecture/Screen_Recording_20220210-171846_Expo Go_4.gif "Mobile simple")



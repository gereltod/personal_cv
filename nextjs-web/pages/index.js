import React from 'react'

// components

import CardPageVisits from '../components/Cards/CardPageVisits.js'
import CardSocialTraffic from '../components/Cards/CardSocialTraffic.js'

// layout for page

import Admin from '../layouts/Admin.js'

export default function Home() {
  return (
    <>
      <div className="flex flex-wrap">
        <div className="mb-12 w-full px-4 xl:mb-0 xl:w-8/12"></div>
        <div className="w-full px-4 xl:w-4/12"></div>
      </div>
      <div className="mt-4 flex flex-wrap">
        <main className="profile-page py-24">
          <section className="bg-blueGray-200 py-24">
            <div className="container mx-auto pt-12 md:pt-24">
              <div className="relative mb-6 -mt-64 flex w-full min-w-0 flex-col break-words rounded-lg bg-white shadow-xl">
                <div className="px-6">
                  <div className="flex flex-wrap justify-center">
                    <div className="flex w-full justify-center px-4 lg:order-2 lg:w-3/12">
                      <div className="relative"></div>
                    </div>
                    <div className="w-full px-4 lg:order-3 lg:w-4/12 lg:self-center lg:text-right">
                      <div className="mt-32 py-6 px-3 sm:mt-0">
                        <button
                          className="mb-1 rounded bg-blueGray-700 px-4 py-2 text-xs font-bold uppercase text-white shadow outline-none transition-all duration-150 ease-linear hover:shadow-md focus:outline-none active:bg-blueGray-600 sm:mr-2"
                          type="button"
                        >
                          Connect
                        </button>
                      </div>
                    </div>
                    <div className="w-full px-4 lg:order-1 lg:w-4/12">
                      <div className="flex justify-center py-4 pt-8 lg:pt-4">
                        <div className="mr-4 p-3 text-center">
                          <span className="block text-xl font-bold uppercase tracking-wide text-blueGray-600">
                            22
                          </span>
                          <span className="text-sm text-blueGray-400">
                            Friends
                          </span>
                        </div>
                        <div className="mr-4 p-3 text-center">
                          <span className="block text-xl font-bold uppercase tracking-wide text-blueGray-600">
                            10
                          </span>
                          <span className="text-sm text-blueGray-400">
                            Photos
                          </span>
                        </div>
                        <div className="p-3 text-center lg:mr-4">
                          <span className="block text-xl font-bold uppercase tracking-wide text-blueGray-600">
                            89
                          </span>
                          <span className="text-sm text-blueGray-400">
                            Comments
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="mt-12 text-center">
                    <h3 className="mb-2 mb-2 text-4xl font-semibold leading-normal text-blueGray-700">
                      Jenna Stones
                    </h3>
                    <div className="mt-0 mb-2 text-sm font-bold uppercase leading-normal text-blueGray-400">
                      <i className="fas fa-map-marker-alt mr-2 text-lg text-blueGray-400"></i>{' '}
                      Los Angeles, California
                    </div>
                    <div className="mb-2 mt-10 text-blueGray-600">
                      <i className="fas fa-briefcase mr-2 text-lg text-blueGray-400"></i>
                      Solution Manager - Creative Tim Officer
                    </div>
                    <div className="mb-2 text-blueGray-600">
                      <i className="fas fa-university mr-2 text-lg text-blueGray-400"></i>
                      University of Computer Science
                    </div>
                  </div>
                  <div className="mt-10 border-t border-blueGray-200 py-10 text-center">
                    <div className="flex flex-wrap justify-center">
                      <div className="w-full px-4 lg:w-9/12">
                        <p className="mb-4 text-lg leading-relaxed text-blueGray-700">
                          An artist of considerable range, Jenna the name taken
                          by Melbourne-raised, Brooklyn-based Nick Murphy
                          writes, performs and records all of his own music,
                          giving it a warm, intimate feel with a solid groove
                          structure. An artist of considerable range.
                        </p>
                        <a
                          href="#pablo"
                          className="font-normal text-lightBlue-500"
                          onClick={(e) => e.preventDefault()}
                        >
                          Show more
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </main>
      </div>
    </>
  )
}

Home.layout = Admin

import React from 'react'

// components

import CardTable from '../../components/Cards/CardTable'
import CardSocialTraffic from '../../components/Cards/CardSocialTraffic.js'

// layout for page

import Admin from '../../layouts/Admin.js'

export default function Google() {
  return (
    <>
      <div className="flex flex-wrap">
        <div className="mb-12 w-full px-4 xl:mb-0 xl:w-8/12">bbb</div>
        <div className="w-full px-4 xl:w-4/12"></div>
        <CardTable name={'Google Cloud Platform'} />
      </div>
      <div className="mt-4 flex flex-wrap">
        <div className="mb-12 w-full px-4 xl:mb-0 xl:w-8/12"></div>
        <div className="w-full px-4 xl:w-4/12"></div>
        teset
      </div>
    </>
  )
}

Google.layout = Admin

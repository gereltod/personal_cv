import React from 'react'

import Admin from '../../layouts/Admin.js'
import CardTable from '../../components/Cards/CardTable.js'

export default function Aws() {
  return (
    <>
      <div className="flex flex-wrap">
        <div className="mb-12 w-full px-4 xl:mb-0 xl:w-8/12"></div>
        <div className="w-full px-4 xl:w-4/12"></div>
        <CardTable name={'Amazon Web Service'} />
      </div>
      <div className="mt-4 flex flex-wrap">
        <div className="mb-12 w-full px-4 xl:mb-0 xl:w-8/12"></div>
        <div className="w-full px-4 xl:w-4/12"></div>
      </div>
    </>
  )
}

Aws.layout = Admin

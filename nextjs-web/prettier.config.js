module.exports = {
  arrowParens: 'always',
  singleQuote: true,
  tabWidth: 2,
  semi: false,
  tailWindConfig: './tailwind.config.js',
}
